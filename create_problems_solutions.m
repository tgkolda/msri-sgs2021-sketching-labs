%% Tiny example
M = ktensor({[1 -1 0; 0 1 -1; 0 1 1], [1 0 1; 0 1 1; -1 1 -1; -1 1 0], [1 1 -1; -1 0 1]});
X = tensor([1 1 -1 -1 2 0 -2 0 2 -2 1 1 -1 -1 1 0 -1 1 1 1 -1 1 0 0],[3 4 2]);
norm(full(M)-X)

%% Run this a bunch of times and check results
M2 = cp_als(X,3);
score(M,M2)

%% Larger Example
d = 3;
sz = [10 8 6];
r = 2;

A = cell(3,1);
for k = 1:d
    A{k} = randn(sz(k),r);
end

M = ktensor(A);
X = full(M);

%% Run this a bunch of times and check results
M2 = cp_als(X,r);
score(M2,M,'lambda_penalty',false)

%% Add noise
eta = 0.1;
R = tensor(@randn,size(M));
Xtrue = full(M);
X = Xtrue + eta * (norm(Xtrue) / norm(R)) * R;

%% Run this a bunch of times and check results
M2 = cp_als(X,r);
score(M2,M,'lambda_penalty',false)


%% High congruence
d = 3;
sz = [100 80 60];
r = 5;
c = 0.8;
A = cell(3,1);
for k = 1:d
    A{k} = matrandcong(sz(k),r,c);
end
M = ktensor(A);

%% Add noise
eta = 0.1;
R = tensor(@randn,size(M));
Xtrue = full(M);
X = Xtrue + eta * (norm(Xtrue) / norm(R)) * R;

%% Run this a bunch of times and check results
M2 = cp_als(X,r);
score(M2,M,'lambda_penalty',false)

%% Nonnegative and Larger Example
d = 3;
sz = [200 150 50];
r = 5;

A = cell(3,1);
for k = 1:d
    A{k} = rand(sz(k),r);
end

M = ktensor(A);
X = full(M);

%% Run this a bunch of times and check results
M2 = cp_als(X,r);
score(M2,M,'lambda_penalty',false)

%% Add noise
eta = 0.1;
R = tensor(@randn,size(M));
Xtrue = full(M);
X = Xtrue + eta * (norm(Xtrue) / norm(R)) * R;

%% Run this a bunch of times and check results
M2 = cp_als(X,r);
score(M2,M,'lambda_penalty',false)
