# Tensor Least Squares Problems for for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

We try a few exercises related to the least squares problems.

- For a tensor of size $`n_1 \times n_2 \times \cdots times n_d`$, 
  show that the complexity of the mode-k least squares solve is
  $`rNn + rN + r^2\left( \sum_{k-1}^d n_k + 1 \right) + r^3`$
  where $`N = n_1 \cdots n_{k-1} n_{k+1} \cdots n_d`$ and $`n = n_k`$.
  
  
- For a matrix of size $`N \times r`$, prove the coherence satisfies $`r/N \leq \mu(A) \leq 1`$.

- Prove the following
  ```math
  \mu(A \otimes B) = \mu(A)\mu(B)
  ```

- Prove the following
  ```math
  \mu(A \odot B) \leq \mu(A)\mu(B)
  ```

- For a 3-tensor of size $`m \times n \times p`$ and a linear index $`\ell \in [mnp]`$,
  what are the formulas for the corresponding $`(i,j,k)`$ tuples?
  
  
