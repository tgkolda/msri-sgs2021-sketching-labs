# Working with Tensors for for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

We try a few exercises in tensor manipulation.

- Consider the following tensor. 

  ```math
  X(:,:,1) = \begin{bmatrix} 3 & 9 & 1 \\ 8 & 2 & 1 \\ 4 & 3 & 9 \end{bmatrix},
  X(:,:,2) = \begin{bmatrix} 6 & 9 & 5 \\ 5 & 6 & 4 \\ 1 & 4 & 1 \end{bmatrix}
  ```

  - [ ] What is its size?
  - [ ] What is the mapping from (i,j,k) to a linear index? 
  - [ ] What is the mapping from (i,j,k) to entry (j,l) in the mode-2 unfolding? Write down the mode-2 unfolding.
  - [ ] What is the mapping from (i,j,k) to entry (k,l) in the mode-3 unfolding? Write down the mode-3 unfolding.
  
       
            
- Here are some properties of the Kronecker product
  ```math
  (B \otimes A)' = B' \otimes A'
  ```
  ```math
  (B \otimes A)(D \otimes C) = (BD) \otimes (AC)
  ```
  ```math
  (B \otimes A)^+ = B^+ \otimes A^+
  ```
  - [ ] Prove 
  ```math
  (B \odot A)'(B \odot A) = B'B \ast A'A
  ```
  - [ ] Prove 
  ```math
  (B \odot A)^+ = (B'B \ast A'A)(B \odot A)'
  ```
  - [ ] Prove 
  ```math
  (B \otimes A)(D \odot C) = (BD) \odot (AC)
  ```

- [ ] If $`M = [[A,B,C]]`$, what is $`M_{(2)}`$? $`M_{(3)}`$?
