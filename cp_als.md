# CP via Alternating Least Squares (CP-ALS) for Tensor Labs for MSRI Summer Graduate School on Sketching, 2021

Tamara G. Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

The purpose of this lab is to get students familiar with ALS and also
introduce the Amino Acids dataset.

## Theory Exercises

* See slides for theory exercises

## Amino Acids dataset

This dataset consists of fluorescence measurements of 5 samples containing 3 amino acids:

* Tryptophan (top)
* Tyrosine (middle)
* Phenylalanine (bottom)

Each amino acid corresponds to a rank-one component. The tensor is of size 5 x 201 x 61 from

* 5 samples
* 201 emission wavelengths
* 61 excitation wavelengths

Please cite the following paper for this data:
Andersen, C. M. & Bro, R.,
**_Practical aspects  of PARAFAC modeling of fluorescence excitation-emission data_**,
Journal  of Chemometrics, 17, 200-215, 2003.

- [ ] Load the data via `load ('amino_acids\claus.mat','X')`
- [ ] Turn `X` into a `tensor` via `X = tensor(X)`
- [ ] Explore the raw data by visualizing the emission-excitation profiles for each of the five samples

## CP-ALS

- [ ] Run `cp_als` on the Amino Acids with rank $`r = 3`$
- [ ] **Bonus** Write your own version of CP-ALS for a 3-way tensor, named
  `cp3_als_<xyz>` where `<xyz>` is replaced by your initials. You can
  call the functions for the tensor and ktensor objects, but don't
  call the existing `cp_als` method.

## Visualizing Results

- [ ] Use `ktensor\viz` to see the results
- [ ] Label the modes
- [ ] Plot the sample mode as a bar graph
- [ ] (Bonus) Make each mode a different color
- [ ] (Bonus) Clean up the spacing

## Interpretting Results

There are three amino acids that _should_ be separated by CP. 

- [ ] How many different amino acids are in Sample 1? In Sample 2? ... In Sample 5?

## Multiple Starts

- [ ] Load a _correct_ answer called `Msoln` via `load ('amino_acids\soln')`
- [ ] How do the results change is the ALS method is initialized with a different starting point?
    - [ ] Look at the final fit value (f)
    - [ ] Visualize the results
    - [ ] Compute the score in comparison to `Msoln`

## Determining Rank

- [ ] Run the same problem a few times each with $`r=2`$ and $`r=4`$.
    - [ ] Look at the final fit value (f)
    - [ ] Visualize the results
    - [ ] Compute the score in comparison to `Msoln`
    


