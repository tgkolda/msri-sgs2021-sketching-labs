%% Load the data
clear
load ('amino_acids/claus.mat','X')
X = tensor(X);

%% Visuzalize the results from one of the five samples
figure(1); clf;
for sidx = 1:5
    subplot(3,2,sidx)
    sample = double(X(sidx,:,:));
    surf(sample);
    xlabel('Excitation')
    ylabel('Emission')
    title(['Sample ' int2str(sidx)]);
end

%% Create your own version of CP-ALS and run
% This solution just uses the TTB version of CP-ALS

M = cp_als(X,3);

%% Visualizing Results

vizopts = {'ModeTitles',{'Sample','Emission','Excitation'},...
    'PlotCommands',{@(x,y) bar(x,y,'g'), @(x,y) plot(x,y,'b'), ...
        @(x,y) plot(x,y,'r')},...
        'BottomSpace',0.07,'HorzSpace',0.05,'LeftSpace',0.08};

viz(M, 'Figure',2, vizopts{:});

%% Load true solution
load('amino_acids/soln.mat')
viz(Msoln, 'Figure',2, vizopts{:});


%% Multiple starts
clear M
for i = 1:5
    M{i} = cp_als(X,3);
    score(M{i},Msoln,'lambda_penalty',false)
    viz(M{i}, 'Figure',3, vizopts{:});
    pause(5)
end

%% R=2
 
clear M
for i = 1:5
    M{i} = cp_als(X,2);
    score(Msoln,M{i},'lambda_penalty',false)
    viz(M{i}, 'Figure',4, vizopts{:});
    pause(5)
end


%% R=4

clear M
for i = 1:5
    M{i} = cp_als(X,4);
    score(M{i},Msoln,'lambda_penalty',false)
    viz(M{i}, 'Figure',5, vizopts{:});
    pause(5)
end
