%% Load in Uber data and set parameters
load ('uber_tensor');
X = uber;

R = 25;
sz = size(uber);
d = ndims(uber);
s = 2^16;

%% Plot leverage scores of random Gaussian factor matrices
Uinit = cell(d,1);
for kk = 1:d
    Uinit{kk} = rand(sz(kk),R);
end

% Calculate and sort the leverage scores
lev = cell(d, 1);

for i = 1:d
   lev{i} = tt_leverage_scores(Uinit{i}); 
   lev{i} = sort(lev{i});
end

% Plot the leverage scores
figure(1)
for i = 1:d
    ax1 = subplot(d, 1, i);
    bar(lev{i})
    set(ax1, 'YScale', 'log')
    q = min(min(lev{i}), 10^(-2));
    set(ax1, 'YLim', [q, 1])
end

%% Run CP-ARLS-Lev and plot the leverage scores of the final factor matrices
rng('shuffle')
sharedparams = {'init', Uinit, 'truefit', 'iter','nsamplsq', s};
[M, ~, info] = cp_arls_lev(X,R,'thresh', [], sharedparams{:});

% Calculate and sort the leverage scores
lev = cell(d, 1);

for i = 1:d
   lev{i} = tt_leverage_scores(M{i}); 
   lev{i} = sort(lev{i});
end

% Plot the leverage scores
figure(2)
for i = 1:d
    ax1 = subplot(d, 1, i);
    bar(lev{i})
    set(ax1, 'YScale', 'log')
    q = max(min(lev{i}), 10^(-10));
    set(ax1, 'YLim', [q, 1])
end

%% Compare various sampling methods
rng('shuffle')
sharedparams = {'init', Uinit, 'truefit', 'iter','nsamplsq', s};

[~, ~, info_uniform] = cp_arls_lev_option(X,R,'thresh', [], 'sampling', 'uniform', sharedparams{:});
[~, ~, info_lengthsqr] = cp_arls_lev_option(X,R,'thresh', [], 'sampling', 'lengthsqr', sharedparams{:});
[~, ~, info_leverage] = cp_arls_lev_option(X,R,'thresh', [], 'sampling', 'leverage', sharedparams{:});

% Zero out traces
numEpochs = info_uniform.iters + 1;
info_uniform.time_trace = info_uniform.time_trace(1:numEpochs);
info_uniform.fit_trace = info_uniform.fit_trace(1:numEpochs);

numEpochs = info_lengthsqr.iters + 1;
info_lengthsqr.time_trace = info_lengthsqr.time_trace(1:numEpochs);
info_lengthsqr.fit_trace = info_lengthsqr.fit_trace(1:numEpochs);

numEpochs = info_leverage.iters + 1;
info_leverage.time_trace = info_leverage.time_trace(1:numEpochs);
info_leverage.fit_trace = info_leverage.fit_trace(1:numEpochs);

% Plot the results
figure(3)
set(gca,'fontsize', 14);
hold on
plot(info_uniform.time_trace, info_uniform.fit_trace, 'linewidth', 2)
plot(info_lengthsqr.time_trace, info_lengthsqr.fit_trace, 'linewidth', 2)
plot(info_leverage.time_trace, info_leverage.fit_trace, 'linewidth', 2)
legend('Uniform', 'Length Squared', 'Leverage', 'Location', 'SouthEast')
hold off



