# CP via Alternating **Randomized** Least Squares (CP-ARLS) for Tensor Labs for MSRI Summer Graduate School on Sketching, 2021

Tamara G. Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

## Compare CP-ALS and CP-ARLS 

Create a $`400 \times 400 \times 400`$ random tensor of rank 5 with correlated factors (congruence = 0.8) and 1% noise.
Compare the time and solution quality for `cp_als` and `cp_arls` (with mixing disabled).

*Note:* Recommended parameteres for `cp_arls` are 
```MATLAB
M = cp_arls(X,r,'mix',false,'epoch',5,'printitn',1);
```
- [ ] Run CP-ALS and CP-ARLS (without mixing)
    - [ ] Compare the timings
    - [ ] Compare the solutions in terms of (true) fit
    - [ ] Do both methods find the true solution?
    - [ ] Do this for a few different starting points

## CP-ARLS with Hazardous Gas Data

This is an interesting data set for tensor analysis. It's relatively
large and has multiway structure. The full details of the data are
described here:

A. Vergara, J. Fonollosa, J. Mahiques, M. Trincavelli, N. Rulkov and R. Huerta,
__On the performance of gas sensor arrays in open sampling systems using Inhibitory Support Vector Machines__,
Sensors and Actuators B: Chemical, 2013, doi:10.1016/j.snb.2013.05.027 

The idea is to analyze chemical sensors used for tracking hazardous
gases. The experiment has a source (shown in red below) that is
released into a chamber. In the data set we use here, the chamber has
71 sensors (really 72, but one was malfunctioning and has been removed
from the data set) at position P3. We collect data for 1000 time steps
and run multiple experiments with 5 temperature and 3 fan
speeds. For each set of conditions, there are 20 experiments per chemical species.

The resulting 5-way tensor has modes:

* 71 sensors
* 1000 time points
* 5 temperatures
* 3 fan speeds
* 60 experiments (3 chemical species x 20 experiments each)

Load the gas data

- [ ] Load `gas/gas3`. (This file also contains `gasvizopts` that can be used with `viz` to display the results of CP.)
- [ ] Run CP-ALS and CP-ARLS (without mixing)
    - [ ] Compare the timings
    - [ ] Compare the solutions in terms of (true) fit, visualization
    - [ ] Do both methods separate the gases?
    - [ ] Can you interpret the factors?
    - [ ] Do this for a few different starting points

