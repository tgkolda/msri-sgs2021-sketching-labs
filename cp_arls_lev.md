# CP-ARLS-LEV for Tensor Labs for MSRI Summer Graduate School on Sketching, 2021

Tamara G. Kolda, tammy.kolda@mathsci.ai and Brett Larsen, bwlarsen@stanford.edu

See [README.md](README.md) for full acknowledgements and other modules.

The purpose of this module is to give the student experience with randomized least squares and using the CP-ARLS-LEV code. It also requires:

- `tensor_toolbox-cp_arls_lev`: This is a version of Tensor Toolbox that has features added beyond the current release.  Remove the `v3.2.1` release using `rmpath` and add this version using `addpath`.
- `uber_tensor.mat`: This is a large sparse tensor of count data for Uber pickups over 6 months in New York City.  The four modes are day, hour, lattitude, longitude.  The raw data is from [FROSTT](http://frostt.io/tensors/uber-pickups/).
- For these experiments we recommend using rank $`R = 25`$ and $`s = 2^{16}`$ samples, but feel free to play with these values.

## Vizualizing Leverage Scores

- [ ] Generate a random initialization to the factor matrices for a CP decomposition of the Uber tensor.  Do this via a Gaussian random matrix; remember that the size of each factor matrix is $`n_k \times R`$.

- [ ] Calculate the leverage scores of each factor matrix (the function `tt_leverage_scores` will do this for you or you can write your own function).  Sort the leverage scores by size and plot them as a bar chart for each mode, using a log scale for the y-axis. What do you observe about the leverage scores for a random initialization?

- [ ] Run CP-ARLS-LEV on the Uber tensor using the function `cp_arls_lev.m`.  Set the arguement `'truefit'` to 'iter' so the true fit is used as the stopping criteria.

- [ ] Sort and plot the leverage scores for the final fit using a log scale for the y-axis.  What do you observe about the final leverage scores as compared to initialization?


## Comparing Sampling Methods

- [ ] For this exercise, use the function `cp_arls_lev_option.m`.  It is identical to `cp_arls_lev.m` except that it allows you to change the sampling method.

- [ ] Pass 'uniform', 'lengthsqr', and 'leverage' to the `'sampling'` arguement to run sketching with these three different sampling methods.  Save out the `info` returned for each.

- [ ] Plot fit vs. time for each of these methods on the same plot.  The data can be found in `info.time_trace` and `info.fit_trace`.  Note that before you plot you should remove the zero padding at the end of these vectors (we don't know how many iterations the algorithm will run for at the beginning).  The following code can be adapted to do this for each sampling method:

	```matlab
	numEpochs = info_uniform.iters + 1;
	info_uniform.time_trace = info_uniform.time_trace(1:numEpochs);
	info_uniform.fit_trace = info_uniform.fit_trace(1:numEpochs);
	```
- [ ] How does the performance of these three sampling methods compare?  Does this match your intuition from lecture?