# MATLAB Basics for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

We briefly cover a few of the basic functions that you'll need to understand 
for the labs that follow.

## Documentation

There are three main ways to get help in MATLAB. 
Typing `doc` launches the help browser in a new window. 
The browser is an interactive, hyperlinked searchable interface to 
MATLAB documentation.

You can also use the `help` function, followed by the name of a function or class, 
to see documentation in the Command Window. 
This documentation contains hyperlinks to class functions, 
related classes and functions, and information about overloaded functions. 
Here's the documentation for the first function you used above:
```matlab
help ones
```
Finally, you can also find documentation via keyword search using the `lookfor`
function. We used the `randn` function above to generate some random data. 
To find documentation related to random numbers in MATLAB, you can search as follows:
```matlab
lookfor random
```

## Arrays

- [ ] Create a vector `x` of all ones and size 3 x 1 using the MATLAB `ones` command
- [ ] Let `A` be the following 3 x 2 matrix, created in MATLAB using square 
  bracket and semicolon (denoting end of row) notation

```math
A = \begin{bmatrix}
  1 & 2 & 3 \\
  4 & 5 & 6
\end{bmatrix}
```

- [ ] Display element $`A(1,2)`$
- [ ] Display row two of $`A`$, i.e., $`A(2,:)`$
- [ ] Display column two of $`A`$, i.e., $`A(:,2)`$
- [ ] Let `B` be the transpose of A using `A'`
- [ ] Let `C` be a $`3 \times 4 \times 2`$ array of normal distributed random 
  numbers using `randn`

## Cell Arrays

So far we've looked at arrays of numbers. A cell array is an arbitrary array of 
objects specified by curly braces. To create a cell array `D` with the four
objects we've already created, type:

```matlab
D = {x,A,B,C};
```

To extract the first element of the cell array, type `D{1}`.

## Function Handles

Here and there we will use function handles, which allow the passing of an 
anonymous function to a method; for instance, we might pass a handle that does 
objective function and gradient evaluations to an optimization routine.

Here is an example of a function that takes two vector (or scalar) inputs and
computes their sum.

```matlab
fsum = @(x,y) x+y;
```

- [ ] Create that function and use it to compute:

```math
\begin{bmatrix}
2 \\ 3 \\ 4
\end{bmatrix}
+
\begin{bmatrix}
1 \\ 7 \\ 2
\end{bmatrix}
```

- [ ] Write a function `fdot` to compute the dot (inner) product between two vectors
- [ ] Use that function to compute

```math
\begin{bmatrix}
2 \\ 3 \\ 4
\end{bmatrix}^T\cdot
\begin{bmatrix}
1 \\ 7 \\ 2
\end{bmatrix}
```

It is okay to create function handles out of existing functions that specify 
some of the input arguments. For instance, we can create a function as follows:
```matlab
econsvd = @(x) svd(x,'econ');
```

- [ ] Write a function called `pow2` that uses the `power` function to compute the 
  elementwise square of an input.
- [ ] Use that function to compute $`5^2`$.

## Timing

Use `tic` and `toc` for simple timing of MATLAB functions. Do not include a 
semicolon after toc to output the timing results.

- [ ] Using the `rand` function (uniform random values in [0,1]), 
  let $`A`$ be a $`500 \times 300`$ matrix
- [ ] Use `tic` and `toc` to time computing the SVD of $`A`$ using the `svd` function
- [ ] (Advanced) For the following code, set up one timer for the overall loop and 
  another to time just the SVD calculation inside each iteration

```matlab
for i = 1:10
  A{i} = rand(i*50,i*50);
  [U{i},S{i},V{i}] = svd(A{i});
end
```

## Workspace Information

- [ ] Type `whos` to see what variables are in the current workspace
= [ ] Type `clear` to clear the workspace 

