# Software Setup for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

Here we describe how to download the software. Zip files of each software package
are included here.

## Tensor Toolbox for MATLAB

We'll be using [Version 3.2.1](https://gitlab.com/tensors/tensor_toolbox/-/releases/v3.2.1) of the Tensor Toolbox for MATLAB. For more information, visit http://www.tensortoolbox.org/.

- [ ] Add the appropriate `tensor_toolbox` directory to your path (change the name of the main directory to this if you download the release zip file)
- [ ] Test that it's installed. Create a random tensor via `X = tensor(@randn,[4 3 2])`.

## Save your path!

- [ ] Execute `savepath` to save your path for next time, or just add there three packages each time you start MATLAB.

