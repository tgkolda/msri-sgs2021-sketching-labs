# Mixing and Tensors Problems for for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

We try a few exercises related to tensor mixing problems.

- Kronecker product
  - [ ] Compute the Kronecker product $`B \otimes A`$ of 
    ```math
    A = \begin{bmatrix} 1 & 2 \\  3 & 4\end{bmatrix} \text{ and }
    B = \begin{bmatrix} -1 & 1 \\  2 & -2 \\ 5 & 3 \end{bmatrix}
    ```
- Khatri-Rao product
  - [ ] Using the same matrices as above, compute the Khatri Rao product (KRP) $`B \odot A`$
- Pre-mixing
    - [ ] Let $`X`$ be a tensor of size $`n_1 \times n_2 \times n_3`$. Define 
      ```math
      \hat X = X \times_1 F_1 D_1 \times_2 F_2 D_2 \times_3 F_3 D_3
        ```
        where $`F_k`$ is the $`n_k \times n_k`$ Fast Fourier transform (FFT) and 
        $`D_k`$ is an $`n_k \times n_k`$  diagonal matrix with random $`\pm 1`$ entries on the digaonal.
        Let $`N=n_1n_2`$, and let $`S`$ be an $`N \times N`$ sampling matrix. Prove
        ```math
        (S \hat X_{(3)}^T) D_3 F_3^* = S((F_2 \otimes F_1)((D_2 \otimes D_1)X_{(3)}^T))    
        ```
    - [ ] What is the (one-time) cost to compute $`\hat X`$?
    - [ ] What is the cost to compute $`(S \hat X_{(3)}^T) D_3 F_3^*`$?
    - [ ] What is the cost to compute $`S((F_2 \otimes F_1)((D_2 \otimes D_1)X_{(3)}^T))`$?
    - [ ] Base on these costs, which would you use?


**Key for last problem**

The following identity is useful for the last problem. It is possible to prove it from what you already know, but the proof is not especially instructive so just use this directly...

```math
Y = X \times_1 V_1 \times_2 V_2 \cdots \times_d V_d 
\Leftrightarrow
Y_{(k)} = V_k X_{(k)} ( V_d \otimes \cdots \otimes V_{k+1} \otimes V_{k-1} \otimes \cdots \otimes V_1)^T
```
