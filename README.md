# Tensor Labs for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

Many of this material is taken from prior labs that have been written with the
help of co-presenters **Danny Dunlavy** and **Kina Winoto**. 
Many thanks also to various collagues and interns that have contributed as well.

## Order of Exercises

### Pre-Work

- [ ] [MATLAB Basics](matlab_basics.md)
- [ ] [Software Install](software.md)

### Lab 1

- [ ] [Working with Tensors](lab1_math.md) - no MATLAB, just math problems
- [ ] [CP via Alternating Least Squares (CP-ALS)](cp_als.md) - also introduces Amino Acids dataset and includes nonnegative least squares challenge
- [ ] [Creating Artificial Test Problems](create_problems.md) - includes adding noise, creating high congruency

### Lab 2

- [ ] [Tensor Least Squares Problems](lab2_math.md) - no MATLAB, just math problems
- [ ] [CP via Alternating Randomized Least Squares (CP-ARLS)](cp_arls.md) - also introduces Hazardous Gas dataset


### Lab 3
- [ ] [Mixing and Tensors Problems](lab3_math.md) - no MATLAB, just math problems
- [ ] [CP-ARLS with Mixing (CP-ARLS-MIX)](cp_arls_mix.md)


### Lab 4

- [ ] [Leverage Scores and Tensor Problems](lab4_math.md) - no MATLAB, just math problems
- [ ] [CP-ARLS with Leverage Scores (CP-ARLS-LEV)](cp_arls_lev.md)

