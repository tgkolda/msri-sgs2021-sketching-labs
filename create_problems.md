# Creating Artificial Problems for Tensor Labs for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai 

See [README.md](README.md) for full acknowledgements and other modules.

The purpose of this lab is to develop understanding of the
relationship between the decomposition and the observation and to help
understand how to generate artificial test problems.

## Tiny Example 

- [ ] Create a `ktensor` called `M` with the following factor matrices:
```math
A_1 = \begin{bmatrix} 1 & -1 & 0 \\ 0 & 1 & -1 \\ 0 & 1 & 1 \end{bmatrix}, 
A_2 = \begin{bmatrix} 1 & 0 & 1 \\ 0 & 1 & 1 \\ -1 & 1 & -1 \\ -1 & 1 & 0\end{bmatrix}, 
A_3 = \begin{bmatrix} 1 & 1 & -1 \\ -1 & 0 & 1\end{bmatrix}.
```
- [ ] Create a `tensor` called `X` in MATLAB.
```math
X(:,:,1) = \begin{bmatrix} 1 & -1 & -2 & -2\\ 1 & 2 & 0 & 1\\ -1 & 0 & 2 & 1 \end{bmatrix}, 
X(:,:,2) = \begin{bmatrix} -1 & 0 & 1 & 1\\ -1 & -1 & 1 & 0\\ 1 & 1 & -1 & 0 \end{bmatrix}.
```
- [ ] Computationally show that `M` is a rank-3 factorization of `X` (use the `full` command)

- [ ] Run your CP-ALS code and see if it finds the correct factorization
    - [ ] What is the final fit of the computed model? Is it the same every time you re-run the code?
    - [ ] Is the model the same as the `M` above?
    - [ ] Does the optimization find the same `M` every time?
    - [ ] Does this tensor satisfy the conditions for uniqueness?


## Larger Example

- [ ] Create a 3-way `ktensor` called `M` of size $`10 \times 8 \times 6`$ and rank $`r=2`$ with factors that have standard normal distributed entries (use `randn`).

- [ ] Create a tensor `X = full(M)` and run your CP-ALS code to see if it finds the correct factorization
    - [ ] What is the final fit of the computed model? Is it the same every time you re-run the code?
    - [ ] Is the model the same as the `M` that was used to generate the data?
    - [ ] Does the optimization find the same `M` every time?
    - [ ] Does this tensor satisfy the conditions for uniqueness?

- [ ] Create a noisy version of X by adding $`\eta = 10\%`$ white noise (SNR = 100) according to the following formula where `R` is a tensor of the same size as `full(M)` such that each entry is drawn from a standard normal distribution:
```math
X = M + \eta \frac{ \| M \| } {\| R \|} R
```

- [ ] With this noisy tensor, run CP-ALS a few times
    - [ ] What is the final fit of the computed model? Is it the same every time you re-run the code?
    - [ ] Is the model the same as the `M` that was used to generate the data?
    - [ ] Does the optimization find the same `M` every time?

- [ ] Vary the noise level $`\eta`$ (do multiple runs with different starting points for each datapoint in the plots below)
    - [ ] Plot the relationship between the final fit and $`\eta`$ 
    - [ ] Plot the relationship between the score and $`\eta`$
 
## Higher Congruence Factors

CP problems are harder when the factors have high congruence, i.e., $`c_{ij} = \cos(a_i,a_j)`$ is the congruence between factors i and j. High congurence vectors have congruences close to 1, and these are the hardest problems. Low congruence vectors are close to orthogonal, and these problems are easier.

- [ ] Use `matrandcong` to create high and low congruence factor matrices (all three factor matrices should have the same congruence) for problem of size $`100 \times 80 \times 60`$. Vary the rank from $`r=2`$ to $`r=5`$.

- [ ] Add 10\% noise ($`\eta = 0.1`$) and see how the score (compared to the true `M`) varies with the congruence and the rank.


