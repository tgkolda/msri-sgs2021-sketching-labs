# Leverage Score Sampling for Tensors for for MSRI Summer Graduate School on Sketching, 2021

Tammy Kolda, tammy.kolda@mathsci.ai and Brett Larsen, bwlarsen@stanford.edu

See [README.md](README.md) for full acknowledgements and other modules.

Exercises on leverage score sampling for tensors.

- **[Leverage Score Properties]** For all problems, consider a matrix: 

	```math
	A \in \mathbb{R}^{N \times r}
	```

	- [ ] If the matrix is full rank prove the following.  What is the sum of the leverage scores if the matrix is rank deficient?
	```math
	\sum_{i \in [N]} \ell_i(A) = r
	```
	- [ ] Prove the following upper bound on the leverage scores.  What does it mean if a leverage score is equal to 1? 
	```math
	\ell_i(A) \leq 1 \quad \forall i \in [N]
	```
	- [ ] Prove the following property of the coherence or the maximum leverage score (assuming the matrix is full rank).  Give an interpreation in terms of the column space of the matrix.
	```math
	\mu(Z) = \max_{i \in [N]} \ell_i(A), \quad \mu(A) \in [r/N, 1]
	```
	

- **[Sampling with Approximate Leverage Scores]** Instead of sampling by the leverage scores, consider we instead sample by a distribution that approximates the leverage scores.  Throughout we assume the matrix is full rank such that the sum of the leverage scores is r.  Define the following "misspecification parameter" for the distribution:

	```math
	\beta = \min_{i \in [N]} \frac{p_i r}{\ell_i(A)}
	```
	- [ ] What are the possible values of $`\beta`$?  Do smaller values indicate the sampling distribution is a better or worse match to the leverage score sampling distribution?

	- [ ] Suppose you sample the rows uniformly for an arbitrary least squares problem.  What is the misspecification paramter in the worst case? 

	- [ ] Revisit Ken's slides with the results on "Leverage-Score Sampling Concentration" with applies the matrix Bernstein inequality to derive the samples required for leverage score sampling to yield an embedding.  How do the following quantities in that derivation change when we instead use apprxoimate leverage score sampling?
	```math
	\mathbb{E}[X_k], \: \|X\|_2, \: \mathbb{E}[X_k^2]
	```   

	- [ ] Use the result from the previous problem and the matrix Bernstein inequality to derive how the samples required depends on the misspecification parameter.  Does this suggest sampling by approximate leverage scores is a viable sketching technique?     

	- [ ] Provide a bound on the misspecification parameter for the leverage score approximation for the Khatri-Rao product derived in lecture.

- **[Unbiased Estimators]** For the following two situations, determine how each sample should be re-weighted in order to ensure an unbiased estimator.

	- [ ] The problem considers hybrid sampling in which high probability rows are included deterministically in the sketch (e.g. if the sampling probability is over 0.2). Once these deterministic rows are included you sample from the remaining rows via leverage scores (this is implemented via rejection sampling where we throw out a deterministic row if it is drawn again).  What must the sampled rows be re-weighted by in order to ensure an unbiased estimator in terms of the leverage scores?

	- [ ] For sparse tensors, we often estimate the fit via stratified sampling.  The user specifies $`\alpha \in [0, 1]`$, and if we estimate the fit using $`s`$ samples, we then draw $`\alpha s`$ samples from the zeros of the tensor and $`(1- \alpha)s`$ samples from the non-zeros of the tensors.  How must these samples be weighted to ensure a non-biased estimator?


